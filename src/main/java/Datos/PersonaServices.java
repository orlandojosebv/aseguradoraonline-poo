/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;

import Modelo.Persona;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface PersonaServices {

    public int create(Persona persona);
    public List<Persona> all();
    public Persona selectId(Persona persona);
    public int update(Persona persona);
    public int delete(Persona persona);
            
}
