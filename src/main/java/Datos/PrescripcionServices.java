/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Datos;

import Modelo.Prescripcion;
import java.util.List;

/**
 *
 * @author estudiante
 */
public interface PrescripcionServices {

    public int create(Prescripcion prescripcion);
    public List<Prescripcion> all();
    public Prescripcion selectId(Prescripcion prescripcion);
    public int update(Prescripcion prescripcion);
    public int delete(Prescripcion prescripcion);
            
}
