/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Modelo.Consulta;
import java.util.List;


/**
 *
 * @author estudiante
 */
public interface ConsultaServices{

    public int create(Consulta consulta);
    public List<Consulta> all();
    public Consulta selectId(Consulta consulta);
    public int update(Consulta consulta);
    public int delete(Consulta consulta);
}

